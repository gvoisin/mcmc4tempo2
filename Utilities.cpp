/*
 * SPDX-FileCopyrightText: 2024 Guillaume VOISIN (researcher at LUTH, Observatoire de Paris, PSL, CNRS) <guillaume.voisin@obspm.fr> <astro.guillaume.voisin@gmail.com>
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */
/* This code implements a timing model and fitting procedures aimed at studiyng th triple system J0337+1715 (Ransom et al. 2013)
 *
 * Written by Guillaume Voisin 2017 , LUTh, Observatoire de Paris, PSL Research University (guillaume.voisin@obspm.fr astro.guillaume.voisin@google.com)
 *
 */

#include <iostream>
#include <cstdio>
#include <cmath>
#include <cstring>
//#include "Constants.h"
#include<vector>
#include<valarray>


using namespace std ;



long double pi = acos(-1.);

int min(int m, int n){
    if (m > n)
        return n ;
    else
        return m ;
}

int max(int m, int n){
    if (m > n)
        return m ;
    else
        return n ;
}


void Print_table(long double table[], long int ntable){
    long int i = 0;
    for (i=0 ; i < ntable ; ++i){
        printf("%.19Le\n", table[i]);
    }
    return;
}


void Print_table(double table[], long int ntable){
    long int i = 0;
    for (i=0 ; i < ntable ; ++i){
        printf("%.15e\n", table[i]);
    }
    return;
}


void Print_table(long double table1[], long double table2[], long int ntable){
    long int i = 0;
    for (i=0 ; i < ntable ; ++i){
        printf("%.19Le    %.19Le\n", table1[i], table2[i]);
    }
    return;
}


void Print_table(float table[], long int ntable){
    long int i = 0;
    for (i=0 ; i < ntable ; ++i){
        printf("%f\n", table[i]);
    }
    return;
}


void Print_table(int table[], int ntable){
    int i = 0;
    for (i=0 ; i < ntable ; ++i){
        printf("%i\n", table[i]);
    }
    return;
}

void Print_table(long int table[], long int ntable){
    long int i = 0;
    for (i=0 ; i < ntable ; ++i){
        printf("%li\n", table[i]);
    }
    return;
}



void Print_table(long long int table[], long int ntable){
    long int i = 0;
    for (i=0 ; i < ntable ; ++i){
        printf("%lli\n", table[i]);
    }
    return;
}


void Print_table(vector<long double> table){
    long int i = 0;
    for (i=0 ; i < table.size() ; ++i){
        printf("%.19Le\n", table[i]);
    }
    return;
}

void Print_table(vector<long int> table){
    long int i = 0;
    for (i=0 ; i < table.size() ; ++i){
        printf("%li\n", table[i]);
    }
    return;
}

void Print_table(vector<int> table){
    long int i = 0;
    for (i=0 ; i < table.size() ; ++i){
        printf("%i\n", table[i]);
    }
    return;
}


void Print_table(valarray<long double> table){
    long int i = 0;
    for (i=0 ; i < table.size() ; ++i){
        printf("%.19Le\n", table[i]);
    }
    return;
}


void Print_table(double ** table, long int ntable, int ncols)
{
  long int i = 0;
  long int j = 0;
  for (i=0 ; i < ntable ; ++i)
  {
    for (j = 0; j < ncols-1 ; j++) printf("%.15e    ", table[i][j]);
    printf("%.15e\n", table[i][ncols-1]);
  }
  return;
}


void sPrint_table(char * str, int table[], int ntable)
{
    int i = 0;
    strcpy(str,"");
    for (i=0 ; i < ntable ; ++i){
        sprintf(str, "%s%i\n", str,table[i]);
    }
    return;
}

long double inversetrigo(long double cosv , long double sinv){

//     cdef np.ndarray cosv, sinv
//     cdef np.ndarray v
//     cdef int i = 0

//     if (type(cosx) != np.ndarray):
//         cosv=np.array([0.], dtype=DTYPE)
//         sinv= np.array([0.], dtype=DTYPE)     // to be sure there is no random tail in the numbers
//         cosv=np.array([cosx], dtype=DTYPE)
//         sinv= np.array([sinx], dtype=DTYPE)
//     else:
//         cosv=np.zeros([len(cosx)], dtype=DTYPE)
//         sinv= np.zeros([len(sinx)], dtype=DTYPE)
//         cosv = cosx
//         sinv = sinx

    long double v = 0 ;

//     for i in range(len(cosv)) :
        if ( abs(sinv ) > 1 or abs(cosv) > 1 ) {
            cout << "inversetrigo : Sinus ou cosinus supérieur à 1 !!!sin = " << sinv << " et cos = " << cosv << endl ;
            sinv = fmin(sinv, 1);
            sinv = fmax(sinv, -1);
            cosv = fmin(cosv, 1);
            cosv = fmax(cosv, -1);
            cout << "inversetrigo : utilise sin = " << sinv << " et cos = " << cosv << endl ;
        }
        if (sinv >= 0 and cosv >= 0 )
            v = asin( sinv ) ;
        else if (sinv >= 0 and cosv < 0 )
            v = acos( cosv ) ;
        else if (sinv  < 0 and cosv >= 0 )
            v = asin( sinv ) ;
        else if (sinv < 0 and cosv < 0 )
            v = pi + atan( sinv / cosv ) ;

        return v ;
}


void rot2D(long double angle, long double vector[],
           const int index1, const int index2){
    /* fait une rotation de angle sur le vecteur vecteur2D sur les composantes données par indexes
        rot2D[indexes[0]] =    | cos(angle)   -sin(angle)   0    | |x|   avec |x| = |vector[indexes[0]] |
        rot2D[indexes[1]]      | sin(angle)    cos(angle)   0    | |y|        |y|   |vector[indexes[1]] |
        Autre composantes      |   0            0           1    | |z|        |z|   |autres composantes le cas échéant |
    */
    int n = index1 ;
    int m = index2 ;
    int vectorlength = max(n,m) + 1;
    long double vecteur[vectorlength];
    long double rotmat[2][2] = { { cos(angle), -sin( angle ) } , { sin( angle ) , cos( angle ) } } ;

    memcpy(vecteur, vector, sizeof(long double) * vectorlength ) ;

    vector[n] = rotmat[0][0]*vecteur[n] + rotmat[0][1]*vecteur[m] ;
    vector[m] = rotmat[1][0]*vecteur[n] + rotmat[1][1]*vecteur[m] ;

    return ;
}



void Savetxt_L(char * filename, valarray<long double> table, int nlines ) {
    int i , j;
    FILE * myfile ;
    myfile = fopen(filename, "w") ;
    for(i=0 ; i < nlines ; ++i)
    {
        fprintf(myfile, "%.19Le\n", table[i] );
    }
    fclose(myfile);
}


void Savetxt_L(char * filename, long double * table, int nlines ) {
    int i , j;
    FILE * myfile ;
    myfile = fopen(filename, "w") ;
    for(i=0 ; i < nlines ; ++i)
    {
        fprintf(myfile, "%.19Le\n", table[i] );
    }
    fclose(myfile);
}

void Savetxt_L(char * filename, long double ** table, int nlines, int nrows ) {
    int i , j;
    FILE * myfile ;
    myfile = fopen(filename, "w") ;
    for(i=0 ; i < nlines ; ++i) {
        for (j = 0; j < nrows ; ++j ) {
             fprintf(myfile, "%.19Le    ", table[i][j] );
         }
         fprintf(myfile, "\n") ;
     }
     fclose(myfile);
}

void Savetxt(const char * filename, double ** table, int nlines, int nrows ) {
    int i ,j;
    FILE * myfile ;
    myfile = fopen(filename, "w") ;
    for(i=0 ; i < nlines ; ++i) {
        for (j = 0; j < nrows ; ++j ) {
             fprintf(myfile, "%.15e    ", table[i][j] );
         }
         fprintf(myfile, "\n") ;
     }
     fclose(myfile);
}

void Savetxt(char * filename, double * table, int nlines ) {
    int i ;
    FILE * myfile ;
    myfile = fopen(filename, "w") ;
    for(i=0 ; i < nlines ; ++i)
    {
        fprintf(myfile, "%.15e\n", table[i] );
    }
    fclose(myfile);
}

void Appendtxt(const char * filename, double ** table, int nlines, int nrows ) {
    int i ,j;
    FILE * myfile ;
    myfile = fopen(filename, "a") ;
    for(i=0 ; i < nlines ; ++i) {
        for (j = 0; j < nrows ; ++j ) {
             fprintf(myfile, "%.15e    ", table[i][j] );
         }
         fprintf(myfile, "\n") ;
     }
     fclose(myfile);
}

void Appendtxt(const char * filename, long double ** table, int nlines, int nrows ) {
    int i ,j;
    FILE * myfile ;
    myfile = fopen(filename, "a") ;
    for(i=0 ; i < nlines ; ++i) {
        for (j = 0; j < nrows ; ++j ) {
             fprintf(myfile, "%.19Le    ", table[i][j] );
         }
         fprintf(myfile, "\n") ;
     }
     fclose(myfile);
}

void Loadtxt(char * filename, double * table, int nlines ) {
    int i ;
    const int sizechar = 50;
    char line[sizechar];
    FILE * myfile ;
    myfile = fopen(filename, "r") ;
//     for(i=0 ; i < nlines ; ++i)
    i=0;
    while (fgets(line, sizechar, myfile) != NULL && i < nlines)
    {
        sscanf(line, "%le", &table[i] );
        i++;
    }
    fclose(myfile);
}

void Loadtxt(char * filename, double ** table, int nlines, int ncols ) {
    int i,j ;
    const int sizechar = 50;
    char line[sizechar];
    FILE * myfile ;
    myfile = fopen(filename, "r") ;
    for (i=0; i < nlines ; i++)
    {
        for (j = 0 ; j < ncols-1; j++) fscanf(myfile, "%le ", &table[i][j] );
        fscanf(myfile, "%le\n", &table[i][j] );
    }
    fclose(myfile);
}


void Arange(long double table[], int ntable ,
            long double firstelement =0.L, long double spacing = 1.L ){
    int i ;
    for (i = 0 ; i < ntable ; ++i) {
       table[i] = i * spacing + firstelement ;
    }
}


void Correlate(double * var1, double * var2, double* correlation, int size, int size_correlation)
{
    int k = 0;
    double mean1 =0.;
    double mean2 = 0.;

    for (k=0; k < size ; k++)
    {
        mean1 += var1[k];
        mean2 += var2[k];
    }
    mean1 /= size;
    mean2 /= size;

    for (int i = 0; i < size_correlation ; i++)
    {
        correlation[i] = 0;
        for (k = 0; k + i < size ; k ++)
        {
            correlation[i] += double((var1[k] - mean1) * (var2[k+i] - mean2));
        }
        correlation[i] /= (size -i);
        correlation[i] /= correlation[0];
    }
}



// void rad2dms(long double rad, long double & d, long double & m, long double & s)
// {
//     long double angle = abs(rad) ;
//     long double minute = raddeg / 60.L ;
//     long double seconde = minute / 60.L ;
//     d = floor(angle / raddeg) ;
//     m = floor( (angle - d * raddeg) / minute) ;
//     s = ( angle - d*degre - m * minute ) / seconde ;
// }



int Maxofarray(double * array, int size)
{
  double maxvalue = array[0];
  int maxindex = 0;
  for (int k = 1; k < size ; k++)
  {
    if (array[k] > maxvalue) maxindex = k;
  }
  return maxindex;
}

// void Maxofarray(double * array, int size, double& maxvalue, int& maxindex)
// {
//   maxindex = 0;
//   for (int k = 1; k < size ; k++)
//   {
//     if (array[k] > maxvalue) maxindex = k;
//   }
//   maxvalue = array[maxindex];
// }


void Swap(int & val1, int & val2)
{
  int inter = val1;
  val1 = val2;
  val2 = inter;
}



int fnextline(FILE * afile)
// Read afile until the end of the current line. The stream indicator points on the first character of the next line.
// Return first character of next line, or EOF.
{
  int buffer = 0;
  while (buffer != '\n' and buffer != EOF) buffer = fgetc(afile);
  return buffer;
}
