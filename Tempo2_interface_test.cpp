/*
 * SPDX-FileCopyrightText: 2024 Guillaume VOISIN (researcher at LUTH, Observatoire de Paris, PSL, CNRS) <guillaume.voisin@obspm.fr> <astro.guillaume.voisin@gmail.com>
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */
#include <iostream>
#include <chrono>
#include <random>
#include <cfloat>
#include <stdio.h>
#include <string.h>
#include "Tempo2_interface.h"

int main ( int argc, char *argv[] )
{
    if (argc != 3) printf("Syntax is : 't2interface parfile timfile'");
    fctTempo2 t2interf;
    t2interf.Load(argv[1], argv[2]);
    printf("\nLnposterior = %.5g\n", t2interf());
    t2interf.Print_MCMC();
    if (argc > 3)  t2interf.Save_residuals(argv[3]);
}
